import { Component, OnInit } from "@angular/core";
import { AuthService } from "../user/auth.service";

@Component({
  selector: "nav-bar",
  templateUrl: "./navbar.component.html",
  styles: [
    `
      li > a.active {
        color: red;
      }
    `,
  ],
})
export class NavBarComponent implements OnInit {
  constructor(public auth: AuthService) {}

  ngOnInit() {}
}
